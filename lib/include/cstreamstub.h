/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of libcstreamstub. */

/* libcstreamstub is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* libcstreamstub is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with libcstreamstub.  If not, see
 * <http://www.gnu.org/licenses/>. */

/**
 * \file  cstreamstub.h
 * \brief Defines cstreamstub interface.
 *
 */

#ifndef CSTREAMSTUB_H
#define CSTREAMSTUB_H

#include <stdio.h>
#include <stdbool.h>

#if __STDC_VERSION__ >= 199901L
/*C99*/
int __isoc99_vfscanf(FILE *stream, const char *format, va_list ap);
int __isoc99_fscanf(FILE *fp, const char *fmt, ...);
#define vfscanf __isoc99_vfscanf
#define fscanf  __isoc99_fscanf
#endif

/**
 * \struct stub_funcs
 * Functions that can be stubed.
 */
struct stub_funcs {
        /** to stub tmpfile */
        FILE * (*tmpfile)(void);
        /** to stub fopen */
        FILE * (*fopen)(const char *path, const char *mode);
        /** to stub freopen */
        FILE *(*freopen)(const char *path, const char *mode, FILE *stream);
        /** to stub fdopen */
        FILE *(*fdopen)(int fd, const char *mode);
        /** to stub fclose */
        int (*fclose)(FILE *stream);
        /** to stub popen  */
        FILE *(*popen)(const char *command, const char *type);
        /** to stub pclose */
        int (*pclose)(FILE *stream);
        /** to stub fmemopen */
        FILE *(*fmemopen)(void *buf, size_t size, const char *mode);
        /** to stub open_memstream */
        FILE *(*open_memstream)(char **ptr, size_t *sizeloc);
        /** to stub fflush */
        int (*fflush)(FILE *stream);
        /** to stub fread */
        size_t (*fread)(void *ptr, size_t size,
                        size_t nmemb, FILE *stream);
        /** to stub fwrite */
        size_t (*fwrite)(const void *ptr, size_t size,
                         size_t nmemb, FILE *stream);
        /** to stub setbuf */
        void (*setbuf)(FILE *stream, char *buf);
        /** to stub setbuffer */
        void (*setbuffer)(FILE *stream, char *buf, size_t size);
        /** to stub setlinebuf */
        void (*setlinebuf)(FILE *stream);
        /** to stub setvbuf */
        int (*setvbuf)(FILE *stream, char *buf, int mode, size_t size);
        /** to stub vfprintf */
        int (*vfprintf)(FILE *stream, const char *format, va_list ap);
        /** to stub fgetc */
        int (*fgetc)(FILE *stream);
        /** to stub fgets */
        char *(*fgets)(char *s, int size, FILE *stream);
        /** to stub ungetc */
        int (*ungetc)(int c, FILE *stream);
        /** to stub fputc */
        int (*fputc)(int c, FILE *stream);
        /** to stub fputs */
        int (*fputs)(const char *s, FILE *stream);
        /** to stub fseek */
        int (*fseek)(FILE *stream, long offset, int whence);
        /** to stub ftell */
        long (*ftell)(FILE *stream);
        /** to stub rewind */
        void (*rewind)(FILE *stream);
        /** to stub fgetpos */
        int (*fgetpos)(FILE *stream, fpos_t *pos);
        /** to stub fsetpos */
        int (*fsetpos)(FILE *stream, const fpos_t *pos);
        /** to stub clearerr */
        void (*clearerr)(FILE *stream);
        /** to stub feof */
        int (*feof)(FILE *stream);
        /** to stub ferror */
        int (*ferror)(FILE *stream);
        /** to stub fileno */
        int (*fileno)(FILE *stream);
        /** to stub fseeko */
        int (*fseeko)(FILE *stream, off_t offset, int whence);
        /** to stub ftello */
        off_t (*ftello)(FILE *stream);
        /** to stub vfscanf */
        int (*vfscanf)(FILE *stream, const char *format, va_list ap);
        /** to stub getw */
        int (*getw)(FILE *stream);
        /** to stub putw */
        int (*putw)(int w, FILE *stream);
};

/**
 * \fn cstreamstub_set_funcs(struct stub_funcs *new_stub_funcs);
 * \brief Function to initialize stubs.
 *
 * \param new_stub_funcs the stub functions to setup.
 *
 */
void cstreamstub_set_funcs (struct stub_funcs *new_stub_funcs);

/**
 * \struct stub_config
 * To desicribe when fopen and other stream creation functions will
 * activate stubbing
 */
struct stub_config {
        struct {
                /** systematic stub activation */
                bool always;
                /** stub activation when "path" parameter of fopen is
		    prefixed by USE_STUB */
                bool with_path_prefix;
                /** stub activation when "mode" parameter of fopen is
		    prefixed by USE_STUB */
                bool with_mode_prefix;
                /** stub activation when callback return true */
                bool with_callback;
                /** callback to validate stub activation */
                bool (*callback)(const char *name,
                                 const char *mode);
        } activation; /**< configuration of when a stream will be
				   stubed or not*/
};

/**
 * \fn cstreamstub_set_config(struct stub_config *new_stub_config);
 * \brief Function to configure stubbing.
 *
 * \param new_stub_config the config to use.
 *
 */
void cstreamstub_set_config(
        struct stub_config *new_stub_config);

/**
 * \def USE_STUB
 * \brief Have to be a prefix of name in fopen call to enable the
 * stubs.
 */
#define USE_STUB "~~~~cstreamstub~~~~"


#endif                          /* CSTREAMSTUB_H */
