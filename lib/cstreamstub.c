/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of libcstreamstub. */

/* libcstreamstub is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* libcstreamstub is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with libcstreamstub.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include "cstreamstub_config.h"

#define _GNU_SOURCE
#include <dlfcn.h>
#include <malloc.h>
#include <string.h>
#include <stdarg.h>

#include "cstreamstub.h"

#if HAVE_UCLOG == 1
#define LOG_MODULE                "cstreamstub"
#include "uclog/uclog.h"
#else                           /* HAVE_UCLOG == 1 */
#define LERROR(format, VARGS...)   fprintf(stderr, format "\n", ## VARGS)
#endif                          /* HAVE_UCLOG == 1 */

static struct stub_funcs stub_funcs;
static struct stub_funcs real_funcs;
static struct stub_config stub_config = {
        .activation.always = false,
        .activation.with_path_prefix = false,
        .activation.with_mode_prefix = false,
        .activation.with_callback = false,
        .activation.callback = NULL,
};

void cstreamstub_set_funcs(
        struct stub_funcs *new_stub_funcs)
{
        if (new_stub_funcs == NULL)
                memset(&stub_funcs, 0, sizeof(stub_funcs));
        else
                memcpy(&stub_funcs, new_stub_funcs, sizeof(stub_funcs));
}

void cstreamstub_set_config(
        struct stub_config *new_stub_config)
{
        if (new_stub_config == NULL)
                memset(&stub_config, 0, sizeof(stub_config));
        else
                memcpy(&stub_config, new_stub_config, sizeof(stub_config));
}

/**
 * \struct stub_file_descriptor
 * Fake FILE * allowing to store cstreamstub internals.
 */
struct stub_file_descriptor {
        FILE *stream;           /**< the real FILE * */
        unsigned int magic;     /**< to be sure that this is stubed stream */
};

#define MAGIC   0xdeadbeef

#if !defined RTLD_NEXT && defined __CYGWIN__
/* WARNING : This is not the value of RTLD_NEXT in linux dlfcn.h */
/*           This is the value which works with CYGWIN  */
#define RTLD_NEXT       ((void *)0)
#endif

#define INIT_REAL_FUNC_FOR(_THE_FUNC_, ERROR_RETURN)                    \
do {                                                                    \
        if (real_funcs._THE_FUNC_ == NULL) {                            \
                real_funcs._THE_FUNC_ = dlsym(RTLD_NEXT, #_THE_FUNC_);  \
                if (real_funcs._THE_FUNC_ == NULL) {                    \
                        LERROR("dlsym error (" #_THE_FUNC_ "): %s",     \
                               dlerror());                              \
                        return ERROR_RETURN;                            \
                }                                                       \
        }                                                               \
} while (0)

#define INIT_REAL_FUNC_FOR_NO_RETURN(_THE_FUNC_)                        \
do {                                                                    \
        if (real_funcs._THE_FUNC_ == NULL) {                            \
                real_funcs._THE_FUNC_ = dlsym(RTLD_NEXT, #_THE_FUNC_);  \
                if (real_funcs._THE_FUNC_ == NULL) {                    \
                        LERROR("dlsym error (" #_THE_FUNC_ "): %s",     \
                               dlerror());                              \
                        return;                                         \
                }                                                       \
        }                                                               \
} while (0)

static bool opening_have_to_be_stubbed(
        const char *path,
        const char *mode);

static bool opening_have_to_be_stubbed(
        const char *path,
        const char *mode)
{
        if (stub_config.activation.always)
                return true;

        if (stub_config.activation.with_path_prefix && (path != NULL)
            && (strncmp(path, USE_STUB, strlen(USE_STUB)) == 0))
                return true;

        if (stub_config.activation.with_mode_prefix && (mode != NULL)
            && (strncmp(mode, USE_STUB, strlen(USE_STUB)) == 0))
                return true;

        if (stub_config.activation.with_callback
            && (stub_config.activation.callback != NULL))
                return stub_config.activation.callback(path, mode);

        return false;
}

#define DEFINE_OPEN_STDIO_FUNC(F_NAME, ARGS_DEF, TEST_PARAM,    \
                               ARGS_CALL...)                    \
FILE *F_NAME ARGS_DEF                                           \
{                                                               \
        INIT_REAL_FUNC_FOR(F_NAME, NULL);                       \
                                                                \
        if ((stub_funcs.F_NAME != NULL) &&                      \
            opening_have_to_be_stubbed TEST_PARAM) {            \
                struct stub_file_descriptor *stub_fd =          \
                        malloc(sizeof(*stub_fd));               \
                                                                \
                stub_fd->magic = MAGIC;                         \
                stub_fd->stream = stub_funcs.F_NAME(ARGS_CALL); \
                                                                \
                if (stub_fd->stream == NULL) {                  \
                        free(stub_fd);                          \
                        stub_fd = NULL;                         \
                }                                               \
                                                                \
                return (FILE *)stub_fd;                         \
        } else {                                                \
                return real_funcs.F_NAME(ARGS_CALL);            \
        }                                                       \
}

DEFINE_OPEN_STDIO_FUNC(tmpfile, (void), (NULL, NULL));

DEFINE_OPEN_STDIO_FUNC(fopen,
                       (const char *path, const char *mode),
                       (path, mode), path, mode);

DEFINE_OPEN_STDIO_FUNC(fdopen,
                       (int fd, const char *mode), (NULL, mode), fd, mode);

DEFINE_OPEN_STDIO_FUNC(freopen,
                       (const char *path, const char *mode, FILE *stream),
                       (path, mode), path, mode, stream);

DEFINE_OPEN_STDIO_FUNC(popen,
                       (const char *command, const char *type),
                       (NULL, NULL), command, type);

DEFINE_OPEN_STDIO_FUNC(fmemopen,
                       (void *buf, size_t size, const char *mode),
                       (NULL, mode), buf, size, mode);

DEFINE_OPEN_STDIO_FUNC(open_memstream,
                       (char **ptr, size_t * sizeloc),
                       (NULL, NULL), ptr, sizeloc);

#define DEFINE_CLOSE_STDIO_FUNC(F_NAME, ARGS_DEF, ERROR_RETURN,         \
                                ARGS_CALL_BUT_FILE...)                  \
int F_NAME ARGS_DEF                                                     \
{                                                                       \
        struct stub_file_descriptor *stub_fd =                          \
                (struct stub_file_descriptor *)stream;                  \
                                                                        \
        INIT_REAL_FUNC_FOR(F_NAME, ERROR_RETURN);                       \
                                                                        \
        if (stub_fd->magic == MAGIC) {                                  \
                if (stub_funcs.F_NAME != NULL) {                        \
                        int return_value =                              \
                                stub_funcs.F_NAME(stub_fd->stream,      \
                                                  ##ARGS_CALL_BUT_FILE); \
                                                                        \
                        free(stub_fd);                                  \
                        return return_value;                            \
                } else {                                                \
                        return real_funcs.F_NAME(stub_fd->stream,       \
                                                 ## ARGS_CALL_BUT_FILE); \
                }                                                       \
        } else {                                                        \
                return real_funcs.F_NAME(stream,                        \
                                         ## ARGS_CALL_BUT_FILE);        \
        }                                                               \
}

DEFINE_CLOSE_STDIO_FUNC(fclose, (FILE *stream), EOF);

DEFINE_CLOSE_STDIO_FUNC(pclose, (FILE *stream), EOF);

#define DEFINE_STDIO_FUNC_POST(RETURN_TYPE, ERROR_RETURN,               \
                               F_NAME, ARGS_DEF, ARGS_CALL_BUT_FILE...) \
RETURN_TYPE F_NAME ARGS_DEF                                             \
{                                                                       \
        struct stub_file_descriptor *stub_fd =                          \
                (struct stub_file_descriptor *)stream;                  \
                                                                        \
        INIT_REAL_FUNC_FOR(F_NAME, ERROR_RETURN);                       \
                                                                        \
        if (stub_fd->magic == MAGIC)                                    \
                if (stub_funcs.F_NAME != NULL)                          \
                        return stub_funcs.F_NAME(ARGS_CALL_BUT_FILE,    \
                                                 stub_fd->stream);      \
                else                                                    \
                        return real_funcs.F_NAME(ARGS_CALL_BUT_FILE,    \
                                                 stub_fd->stream);      \
        else                                                            \
                return real_funcs.F_NAME(ARGS_CALL_BUT_FILE,            \
                                         stream);                       \
}

#define DEFINE_STDIO_FUNC_PRE(RETURN_TYPE, ERROR_RETURN,                \
                              F_NAME, ARGS_DEF, ARGS_CALL_BUT_FILE...)  \
RETURN_TYPE F_NAME ARGS_DEF                                             \
{                                                                       \
        struct stub_file_descriptor *stub_fd =                          \
                (struct stub_file_descriptor *)stream;                  \
                                                                        \
        INIT_REAL_FUNC_FOR(F_NAME, ERROR_RETURN);                       \
                                                                        \
        if (stub_fd->magic == MAGIC)                                    \
                if (stub_funcs.F_NAME != NULL)                          \
                        return stub_funcs.F_NAME(stub_fd->stream,       \
                                                 ## ARGS_CALL_BUT_FILE); \
                else                                                    \
                        return real_funcs.F_NAME(stub_fd->stream,       \
                                                 ## ARGS_CALL_BUT_FILE); \
        else                                                            \
                return real_funcs.F_NAME(stream, ## ARGS_CALL_BUT_FILE); \
}

DEFINE_STDIO_FUNC_POST(size_t, 0,
                       fwrite,
                       (const void *ptr, size_t size,
                        size_t nmemb, FILE *stream), ptr, size, nmemb);

DEFINE_STDIO_FUNC_POST(size_t, 0,
                       fread,
                       (void *ptr, size_t size, size_t nmemb, FILE *stream),
                       ptr, size, nmemb);

DEFINE_STDIO_FUNC_PRE(int,
                      EOF,
                      fflush,
                       (FILE *stream));

#define DEFINE_STDIO_FUNC_PRE_NO_RETURN(F_NAME, ARGS_DEF,               \
                                        ARGS_CALL_BUT_FILE...)          \
void F_NAME ARGS_DEF                                                    \
{                                                                       \
        struct stub_file_descriptor *stub_fd =                          \
                (struct stub_file_descriptor *)stream;                  \
                                                                        \
        INIT_REAL_FUNC_FOR_NO_RETURN(F_NAME);                           \
                                                                        \
        if (stub_fd->magic == MAGIC)                                    \
                if (stub_funcs.F_NAME != NULL)                          \
                        stub_funcs.F_NAME(stub_fd->stream,              \
                                          ## ARGS_CALL_BUT_FILE);       \
                else                                                    \
                        real_funcs.F_NAME(stub_fd->stream,              \
                                          ## ARGS_CALL_BUT_FILE);       \
        else                                                            \
                real_funcs.F_NAME(stream, ## ARGS_CALL_BUT_FILE);       \
        return;                                                         \
}

DEFINE_STDIO_FUNC_PRE_NO_RETURN(setbuf, (FILE *stream, char *buf), buf);

DEFINE_STDIO_FUNC_PRE_NO_RETURN(setbuffer,
                                (FILE *stream, char *buf, size_t size),
                                buf, size);

DEFINE_STDIO_FUNC_PRE_NO_RETURN(setlinebuf, (FILE *stream));

DEFINE_STDIO_FUNC_PRE(int, -1,
                      setvbuf,
                      (FILE *stream, char *buf, int mode, size_t size),
                      buf,
                      mode,
                      size);

/* Need to redefine fprintf: something is going wrong when I use the glibc
 * one - implementation taken from glibc */
int fprintf(FILE *stream, const char *format, ...)
{
        va_list arg;
        int done;

        va_start (arg, format);
        done = vfprintf(stream, format, arg);
        va_end (arg);

        return done;
}

DEFINE_STDIO_FUNC_PRE(int, -1,
                      vfprintf,
                      (FILE *stream, const char *format, va_list ap),
                      format,
                      ap);

DEFINE_STDIO_FUNC_PRE(int, EOF,
                      fgetc,
                      (FILE *stream));

DEFINE_STDIO_FUNC_POST(char *, NULL,
                       fgets,
                       (char *s, int size, FILE *stream),
                       s,
                       size);

DEFINE_STDIO_FUNC_POST(int, EOF,
                       ungetc,
                       (int c, FILE *stream),
                       c);

DEFINE_STDIO_FUNC_POST(int, EOF,
                       fputc,
                       (int c, FILE *stream),
                       c);

DEFINE_STDIO_FUNC_POST(int, EOF,
                       fputs,
                       (const char *s, FILE *stream),
                       s);

DEFINE_STDIO_FUNC_PRE(int, -1,
                      fseek,
                      (FILE *stream, long offset, int whence),
                      offset,
                      whence);

DEFINE_STDIO_FUNC_PRE(long int, -1,
                      ftell,
                      (FILE *stream));

DEFINE_STDIO_FUNC_PRE_NO_RETURN(rewind, (FILE *stream));

DEFINE_STDIO_FUNC_PRE(int, -1,
                      fgetpos,
                      (FILE *stream, fpos_t * pos),
                      pos);

DEFINE_STDIO_FUNC_PRE(int, -1,
                      fsetpos,
                      (FILE *stream, const fpos_t * pos),
                      pos);

DEFINE_STDIO_FUNC_PRE_NO_RETURN(clearerr, (FILE *stream));

DEFINE_STDIO_FUNC_PRE(int, -1,
                      feof,
                      (FILE *stream));

DEFINE_STDIO_FUNC_PRE(int, -1,
                      ferror,
                      (FILE *stream));

DEFINE_STDIO_FUNC_PRE(int, -1,
                      fileno,
                      (FILE *stream));

DEFINE_STDIO_FUNC_PRE(int, -1,
                      fseeko,
                      (FILE *stream, off_t offset, int whence),
                      offset,
                      whence);

DEFINE_STDIO_FUNC_PRE(off_t, -1,
                      ftello,
                      (FILE *stream));

/* Need to redefine fscanf: something is going wrong when I use the glibc
 * one - implementation taken from glibc */
int fscanf(FILE *fp, const char *fmt, ...)
{
        int count;
        va_list ap;

        va_start (ap, fmt);
        count = vfscanf(fp, fmt, ap);
        va_end (ap);
        return (count);
}

DEFINE_STDIO_FUNC_PRE(int, EOF,
                      vfscanf,
                      (FILE *stream, const char *format, va_list ap),
                      format,
                      ap);

DEFINE_STDIO_FUNC_PRE(int, EOF,
                      getw,
                      (FILE *stream));

DEFINE_STDIO_FUNC_POST(int, EOF,
                       putw,
                       (int w, FILE *stream),
                       w);
