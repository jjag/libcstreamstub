/* Copyright 2018 Jérôme Arbez-Gindre */

/* This file is part of libcstreamstub. */

/* libcstreamstub is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version. */

/* libcstreamstub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with libcstreamstub.  If not, see
 * <http://www.gnu.org/licenses/>. */

#ifndef STUB_COLLECTION_H
#define STUB_COLLECTION_H

#include <stdio.h>

FILE *ut_fopen(const char *path, const char *mode);
FILE *ut_tmpfile(void);
int ut_fclose(FILE *stream);
FILE *ut_freopen(const char *pathname, const char *mode, FILE *stream);
FILE *ut_popen(const char *command, const char *type);
int ut_pclose(FILE *stream);
FILE *ut_fmemopen(void *buf, size_t size, const char *mode);
FILE *ut_open_memstream(char **ptr, size_t * sizeloc);
void ut_setbuf(FILE *stream, char *buf);
void ut_setbuffer(FILE *stream, char *buf, size_t size);
void ut_setlinebuf(FILE *stream);
int ut_setvbuf(FILE *stream, char *buf, int mode, size_t size);
FILE *ut_fdopen(int fd, const char *mode);
int ut_fflush(FILE *stream);
size_t ut_fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
size_t ut_fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);
int ut_vfprintf(FILE *stream, const char *format, va_list ap);
int ut_vfscanf(FILE *stream, const char *format, va_list ap);
int ut_fgetc(FILE *stream);
char *ut_fgets(char *s, int size, FILE *stream);
int ut_ungetc(int c, FILE *stream);
int ut_fputc(int c, FILE *stream);
int ut_fputs(const char *s, FILE *stream);
int ut_fseek(FILE *stream, long offset, int whence);
long ut_ftell(FILE *stream);
int ut_fseeko(FILE *stream, off_t offset, int whence);
off_t ut_ftello(FILE *stream);
void ut_rewind(FILE *stream);
int ut_fgetpos(FILE *stream, fpos_t *pos);
int ut_fsetpos(FILE *stream, const fpos_t *pos);
void ut_clearerr(FILE *stream);
int ut_feof(FILE *stream);
int ut_ferror(FILE *stream);
int ut_fileno(FILE *stream);
int ut_getw(FILE *stream);
int ut_putw(int w, FILE *stream);

struct ut {
        struct {
                int ncall;
                const char *path;
                const char *mode;
                FILE *return_value;
        } fopen;

        struct {
                int ncall;
                FILE *return_value;
        } tmpfile;

        struct {
                int ncall;
                FILE *stream;
                int return_value;
        } fclose;

        struct {
                int ncall;
                const char *pathname;
                const char *mode;
                const FILE *stream;
                FILE *return_value;
        } freopen;

        struct {
                int ncall;
                const char *command;
                const char *type;
                FILE *return_value;
        } popen;

        struct {
                int ncall;
                FILE *stream;
                int return_value;
        } pclose;

        struct {
                int ncall;
                void *buf;
                size_t size;
                const char *mode;
                FILE *return_value;
        } fmemopen;

        struct {
                int ncall;
                char **ptr;
                size_t *sizeloc;
                FILE *return_value;
        } open_memstream;

        struct {
                int ncall;
                FILE *stream;
                char *buf;
        } setbuf;

        struct {
                int ncall;
                FILE *stream;
                char *buf;
                size_t size;
        } setbuffer;

        struct {
                int ncall;
                FILE *stream;
        } setlinebuf;

        struct {
                int ncall;
                FILE *stream;
                char *buf;
                int mode;
                size_t size;
                int return_value;
        } setvbuf;

        struct {
                int ncall;
                int fd;
                const char *mode;
                FILE *return_value;
        } fdopen;

        struct {
                int ncall;
                FILE *stream;
                int return_value;
        } fflush;

        struct {
                int ncall;
                void *ptr;
                size_t size;
                size_t nmemb;
                FILE *stream;
                size_t return_value;
        } fread;

        struct {
                int ncall;
                const void *ptr;
                size_t size;
                size_t nmemb;
                FILE *stream;
                size_t return_value;
        } fwrite;

        struct {
                int ncall;
                FILE *stream;
                char *str_result_buffer;
                size_t str_result_buffer_size;
                int return_value;
        } vfprintf;

        struct {
                int ncall;
                FILE *stream;
                const char *read_string;
                int return_value;
        } vfscanf;

        struct {
                int ncall;
                FILE *stream;
                int return_value;
        } fgetc;

        struct {
                int ncall;
                char *s;
                int size;
                FILE *stream;
                char *return_value;
        } fgets;

        struct {
                int ncall;
                int c;
                FILE *stream;
                int return_value;
        } ungetc;

        struct {
                int ncall;
                int c;
                FILE *stream;
                int return_value;
        } fputc;

        struct {
                int ncall;
                const char *s;
                FILE *stream;
                int return_value;
        } fputs;

        struct {
                int ncall;
                FILE *stream;
                long offset;
                int whence;
                int return_value;
        } fseek;

        struct {
                int ncall;
                FILE *stream;
                long return_value;
        } ftell;

        struct {
                int ncall;
                FILE *stream;
                off_t offset;
                int whence;
                int return_value;
        } fseeko;

        struct {
                int ncall;
                FILE *stream;
                off_t return_value;
        } ftello;

        struct {
                int ncall;
                FILE *stream;
        } rewind;

        struct {
                int ncall;
                FILE *stream;
                fpos_t *pos;
                int return_value;
        } fgetpos;

        struct {
                int ncall;
                FILE *stream;
                const fpos_t *pos;
                int return_value;
        } fsetpos;

        struct {
                int ncall;
                FILE *stream;
        } clearerr;

        struct {
                int ncall;
                FILE *stream;
                int return_value;
        } feof;

        struct {
                int ncall;
                FILE *stream;
                int return_value;
        } ferror;

        struct {
                int ncall;
                FILE *stream;
                int return_value;
        } fileno;

        struct {
                int ncall;
                FILE *stream;
                int return_value;
        } getw;

        struct {
                int ncall;
                int w;
                FILE *stream;
                int return_value;
        } putw;
};

struct ut *ut(void);
void init_ut_data(void);

#endif /* STUB_COLLECTION_H */
