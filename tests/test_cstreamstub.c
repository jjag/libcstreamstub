/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of libcstreamstub. */

/* libcstreamstub is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* libcstreamstub is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with libcstreamstub.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <check.h>

#include "cstreamstub.h"
#include "stub_collection.h"

static void setup(void);

static void teardown(void);

struct Suite *get_suite(void);

static struct stub_funcs stub_funcs;
static struct stub_config stub_config;

static void setup(void)
{
        init_ut_data();
        memset(&stub_funcs, 0, sizeof(stub_funcs));
        memset(&stub_config, 0, sizeof(stub_config));
}

static void teardown(void)
{
}

START_TEST(test_init_disabled)
{
        FILE *fopen_return;

        ut()->fopen.return_value = NULL;

        stub_funcs.fopen = ut_fopen;
        cstreamstub_set_funcs(&stub_funcs);

        fopen_return = fopen(USE_STUB "/tmp/pouet", "r");

        ck_assert_int_eq(ut()->fopen.ncall, 0);
        ck_assert_ptr_eq(fopen_return, NULL);
        ck_assert_str_eq(ut()->fopen.path, "");
        ck_assert_str_eq(ut()->fopen.mode, "");
}
END_TEST

START_TEST(test_init_always)
{
        FILE *fopen_return;

        ut()->fopen.return_value= NULL;

        stub_funcs.fopen = ut_fopen;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_ptr_eq(fopen_return, NULL);
        ck_assert_str_eq(ut()->fopen.path, "/tmp/pouet");
        ck_assert_str_eq(ut()->fopen.mode, "r");
}
END_TEST

START_TEST(test_init_with_path_prefix)
{
        FILE *fopen_return;

        ut()->fopen.return_value = NULL;

        stub_funcs.fopen = ut_fopen;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.with_path_prefix = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen(USE_STUB "/tmp/pouet", "r");

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_ptr_eq(fopen_return, NULL);
        ck_assert_str_eq(ut()->fopen.path, USE_STUB "/tmp/pouet");
        ck_assert_str_eq(ut()->fopen.mode, "r");
}
END_TEST

START_TEST(test_init_with_tmpfile)
{
        FILE *fopen_return;

        ut()->fopen.return_value= NULL;

        stub_funcs.tmpfile = ut_tmpfile;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.with_path_prefix = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = tmpfile();

        ck_assert_int_eq(ut()->tmpfile.ncall, 0);
        ck_assert_ptr_ne(fopen_return, NULL);
}
END_TEST

START_TEST(test_init_with_no_path_prefix)
{
        FILE *fopen_return;

        ut()->fopen.return_value= NULL;

        stub_funcs.fopen = ut_fopen;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.with_path_prefix = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("BAD" USE_STUB "/tmp/pouet", "r");

        ck_assert_int_eq(ut()->fopen.ncall, 0);
        ck_assert_ptr_eq(fopen_return, NULL);
        ck_assert_str_eq(ut()->fopen.path, "");
        ck_assert_str_eq(ut()->fopen.mode, "");
}
END_TEST

START_TEST(test_init_with_mode_prefix)
{
        FILE *fopen_return;

        ut()->fopen.return_value= NULL;

        stub_funcs.fopen = ut_fopen;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.with_mode_prefix = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", USE_STUB "r");

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_ptr_eq(fopen_return, NULL);
        ck_assert_str_eq(ut()->fopen.path, "/tmp/pouet");
        ck_assert_str_eq(ut()->fopen.mode, USE_STUB "r");
}
END_TEST

START_TEST(test_init_with_no_mode_prefix)
{
        FILE *fopen_return;

        ut()->fopen.return_value= NULL;

        stub_funcs.fopen = ut_fopen;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.with_mode_prefix = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "BAD" USE_STUB "r");

        ck_assert_int_eq(ut()->fopen.ncall, 0);
        ck_assert_ptr_eq(fopen_return, NULL);
        ck_assert_str_eq(ut()->fopen.path, "");
        ck_assert_str_eq(ut()->fopen.mode, "");
}

END_TEST bool activation_callback(const char *name,
                                  const char *mode);

bool activation_callback(
        const char *name __attribute__ ((unused)),
        const char *mode __attribute__ ((unused)))
{
        return true;
}

START_TEST(test_init_with_callback)
{
        FILE *fopen_return;

        ut()->fopen.return_value= NULL;

        stub_funcs.fopen = ut_fopen;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.with_callback = true;
        stub_config.activation.callback = activation_callback;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_ptr_eq(fopen_return, NULL);
        ck_assert_str_eq(ut()->fopen.path, "/tmp/pouet");
        ck_assert_str_eq(ut()->fopen.mode, "r");
}
END_TEST

START_TEST(test_init_with_bad_callback)
{
        FILE *fopen_return;

        ut()->fopen.return_value = NULL;

        stub_funcs.fopen = ut_fopen;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.with_callback = true;
        stub_config.activation.callback = NULL;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");

        ck_assert_int_eq(ut()->fopen.ncall, 0);
        ck_assert_ptr_eq(fopen_return, NULL);
        ck_assert_str_eq(ut()->fopen.path, "");
        ck_assert_str_eq(ut()->fopen.mode, "");
}
END_TEST

START_TEST(test_init_errors_no_funcs)
{
        FILE *fopen_return;

        cstreamstub_set_funcs(NULL);

        fopen_return = fopen(USE_STUB "/tmp/pouet", "r");

        ck_assert_ptr_eq(fopen_return, NULL);
}
END_TEST

START_TEST(test_init_errors_no_config)
{
        FILE *fopen_return;

        cstreamstub_set_config(NULL);

        fopen_return = fopen(USE_STUB "/tmp/pouet", "r");

        ck_assert_ptr_eq(fopen_return, NULL);
}
END_TEST

START_TEST(test_fopen_fclose)
{
        FILE *fopen_return;

        ut()->fopen.return_value= (FILE *)0x1234;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_ptr_ne(fopen_return, NULL);
        ck_assert_str_eq(ut()->fopen.path, "/tmp/pouet");
        ck_assert_str_eq(ut()->fopen.mode, "r");

        fclose(fopen_return);

        ck_assert_int_eq(ut()->fclose.ncall, 1);
        ck_assert_ptr_eq(ut()->fclose.stream, (void *)0x1234);
}
END_TEST

START_TEST(test_freopen_fclose)
{
        FILE *freopen_return;

        ut()->freopen.return_value = (FILE *)0x1234;

        stub_funcs.fclose = ut_fclose;
        stub_funcs.freopen = ut_freopen;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        freopen_return = freopen("/tmp/pouet", "r", (FILE *)0x4321);

        ck_assert_int_eq(ut()->freopen.ncall, 1);
        ck_assert_ptr_ne(freopen_return, NULL);
        ck_assert_str_eq(ut()->freopen.pathname, "/tmp/pouet");
        ck_assert_str_eq(ut()->freopen.mode, "r");
        ck_assert_ptr_eq(ut()->freopen.stream, (FILE *)0x4321);

        fclose(freopen_return);

        ck_assert_int_eq(ut()->fclose.ncall, 1);
        ck_assert_ptr_eq(ut()->fclose.stream, (void *)0x1234);
}
END_TEST

START_TEST(test_popen_pclose)
{
        FILE *popen_return;

        ut()->popen.return_value = (FILE *)0x1234;

        stub_funcs.popen = ut_popen;
        stub_funcs.pclose = ut_pclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        popen_return = popen("cat /tmp/pouet", "r");

        ck_assert_int_eq(ut()->popen.ncall, 1);
        ck_assert_ptr_ne(popen_return, NULL);
        ck_assert_str_eq(ut()->popen.command, "cat /tmp/pouet");
        ck_assert_str_eq(ut()->popen.type, "r");

        pclose(popen_return);

        ck_assert_int_eq(ut()->pclose.ncall, 1);
        ck_assert_ptr_eq(ut()->pclose.stream, (void *)0x1234);
}
END_TEST

START_TEST(test_fmemopen_fclose)
{
        FILE *fmemopen_return;

        ut()->fmemopen.return_value = (FILE *)0x1234;

        stub_funcs.fmemopen = ut_fmemopen;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fmemopen_return = fmemopen((void *)0x9876, 17, "w");

        ck_assert_int_eq(ut()->fmemopen.ncall, 1);
        ck_assert_ptr_ne(fmemopen_return, NULL);
        ck_assert_ptr_eq(ut()->fmemopen.buf, (void *)0x9876);
        ck_assert_uint_eq(ut()->fmemopen.size, 17);
        ck_assert_str_eq(ut()->fmemopen.mode, "w");

        fclose(fmemopen_return);

        ck_assert_int_eq(ut()->fclose.ncall, 1);
        ck_assert_ptr_eq(ut()->fclose.stream, (void *)0x1234);
}
END_TEST

START_TEST(test_open_memstream_fclose)
{
        FILE *open_memstream_return;

        ut()->open_memstream.return_value = (FILE *)0x1234;

        stub_funcs.open_memstream = ut_open_memstream;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        open_memstream_return = open_memstream((char **)0x9876,
                                               (size_t *) 0x5432);

        ck_assert_int_eq(ut()->open_memstream.ncall, 1);
        ck_assert_ptr_ne(open_memstream_return, NULL);
        ck_assert_ptr_eq(ut()->open_memstream.ptr, (char **)0x9876);
        ck_assert_ptr_eq(ut()->open_memstream.sizeloc, (size_t *) 0x5432);

        fclose(open_memstream_return);

        ck_assert_int_eq(ut()->fclose.ncall, 1);
        ck_assert_ptr_eq(ut()->fclose.stream, (void *)0x1234);
}
END_TEST

START_TEST(test_fdopen_fclose)
{
        ut()->fdopen.return_value = (FILE *)0x1234;
        FILE *fdopen_return;

        stub_funcs.fdopen = ut_fdopen;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.with_mode_prefix = true;
        cstreamstub_set_config(&stub_config);

        fdopen_return = fdopen(123, USE_STUB "r");
        fclose(fdopen_return );

        ck_assert_int_eq(ut()->fdopen.ncall, 1);
        ck_assert_int_eq(ut()->fdopen.fd, 123);
        ck_assert_str_eq(ut()->fdopen.mode, USE_STUB "r");
        ck_assert_ptr_ne(fdopen_return, NULL);

        ck_assert_int_eq(ut()->fclose.ncall, 1);
        ck_assert_ptr_eq(ut()->fclose.stream, (FILE *)0x1234);
}
END_TEST

START_TEST(test_setbuf)
{
        ut()->fopen.return_value= (FILE *)0x1234;
        FILE *fopen_return;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.setbuf = ut_setbuf;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        setbuf(fopen_return, (char *)0x2345);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->setbuf.ncall, 1);
        ck_assert_ptr_eq(ut()->setbuf.stream, (FILE *)0x1234);
        ck_assert_ptr_eq(ut()->setbuf.buf, (char *)0x2345);

        fclose(fopen_return);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_setbuffer)
{
        ut()->fopen.return_value= (FILE *)0x1234;
        FILE *fopen_return;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.setbuffer = ut_setbuffer;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        setbuffer(fopen_return, (char *)0x2345, 17);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->setbuffer.ncall, 1);
        ck_assert_ptr_eq(ut()->setbuffer.stream, (FILE *)0x1234);
        ck_assert_ptr_eq(ut()->setbuffer.buf, (char *)0x2345);
        ck_assert_uint_eq(ut()->setbuffer.size, 17);

        fclose(fopen_return);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_setlinebuf)
{
        ut()->fopen.return_value= (FILE *)0x1234;
        FILE *fopen_return;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.setlinebuf = ut_setlinebuf;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        setlinebuf(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->setlinebuf.ncall, 1);
        ck_assert_ptr_eq(ut()->setlinebuf.stream, (FILE *)0x1234);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_setvbuf)
{
        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->setvbuf.return_value= -13;
        FILE *fopen_return;
        int setvbuf_return;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.setvbuf = ut_setvbuf;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        setvbuf_return = setvbuf(fopen_return, (char *)0x7654, 1234, 1256);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->setvbuf.ncall, 1);
        ck_assert_ptr_eq(ut()->setvbuf.stream, (FILE *)0x1234);
        ck_assert_ptr_eq(ut()->setvbuf.buf, (char *)0x7654);
        ck_assert_int_eq(ut()->setvbuf.mode, 1234);
        ck_assert_uint_eq(ut()->setvbuf.size, 1256);
        ck_assert_int_eq(setvbuf_return, -13);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fflush)
{
        FILE *fopen_return;
        int fflush_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fflush.return_value= 1234;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fclose = ut_fclose;
        stub_funcs.fflush = ut_fflush;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fflush_return = fflush(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fflush.ncall, 1);
        ck_assert_ptr_eq(ut()->fflush.stream, (FILE *)0x1234);
        ck_assert_int_eq(fflush_return, 1234);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fread)
{
        FILE *fopen_return;
        size_t fread_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fread.return_value= 1234;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fclose = ut_fclose;
        stub_funcs.fread = ut_fread;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fread_return = fread((void *)0x9876, 12, 13, fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fread.ncall, 1);
        ck_assert_ptr_eq(ut()->fread.ptr, (void *)0x9876);
        ck_assert_uint_eq(ut()->fread.size, 12);
        ck_assert_uint_eq(ut()->fread.nmemb, 13);
        ck_assert_ptr_eq(ut()->fread.stream, (FILE *)0x1234);
        ck_assert_uint_eq(fread_return, 1234);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fwrite)
{
        FILE *fopen_return;
        size_t fwrite_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fwrite.return_value= 1234;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fclose = ut_fclose;
        stub_funcs.fwrite = ut_fwrite;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fwrite_return = fwrite((const void *)0x9876, 12, 13, fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fwrite.ncall, 1);
        ck_assert_ptr_eq(ut()->fwrite.ptr, (void *)0x9876);
        ck_assert_uint_eq(ut()->fwrite.size, 12);
        ck_assert_uint_eq(ut()->fwrite.nmemb, 13);
        ck_assert_ptr_eq(ut()->fwrite.stream, (FILE *)0x1234);
        ck_assert_uint_eq(fwrite_return, 1234);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_vfprintf)
{
        FILE *fopen_return;
        int fprintf_return;
        char str_result_buffer[10] ;

        ut()->vfprintf.str_result_buffer = str_result_buffer;
        ut()->vfprintf.str_result_buffer_size = sizeof(str_result_buffer);

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->vfprintf.return_value= 1234;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fclose = ut_fclose;
        stub_funcs.vfprintf = ut_vfprintf;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fprintf_return = fprintf(fopen_return, "%d %s",
                                 13, "pouet");
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->vfprintf.ncall, 1);
        ck_assert_ptr_eq(ut()->vfprintf.stream, (FILE *)0x1234);
        ck_assert_str_eq(ut()->vfprintf.str_result_buffer, "13 pouet");
        ck_assert_int_eq(fprintf_return, 1234);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_vfscanf)
{
        FILE *fopen_return;
        int fscanf_return;
        int fscanf_d1;
        int fscanf_d2;

        ut()->vfscanf.read_string = "567 890";

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->vfscanf.return_value= 1234;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fclose = ut_fclose;
        stub_funcs.vfscanf = ut_vfscanf;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fscanf_return = fscanf(fopen_return, "%d %d",
                               &fscanf_d1, &fscanf_d2);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->vfscanf.ncall, 1);
        ck_assert_ptr_eq(ut()->vfscanf.stream, (FILE *)0x1234);
        ck_assert_int_eq(fscanf_d1, 567);
        ck_assert_int_eq(fscanf_d2, 890);
        ck_assert_int_eq(fscanf_return, 1234);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fgetc)
{
        FILE *fopen_return;
        int fgetc_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fgetc.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fgetc = ut_fgetc;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fgetc_return = fgetc(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fgetc.ncall, 1);
        ck_assert_ptr_eq(ut()->fgetc.stream, (FILE *)0x1234);
        ck_assert_int_eq(fgetc_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fgets)
{
        FILE *fopen_return;
        char *fgets_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fgets.return_value = (char *)0x6789;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fgets = ut_fgets;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fgets_return = fgets((char *)0x5432, 124, fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fgets.ncall, 1);
        ck_assert_ptr_eq(ut()->fgets.s, (char *)0x5432);
        ck_assert_int_eq(ut()->fgets.size, 124);
        ck_assert_ptr_eq(ut()->fgets.stream, (FILE *)0x1234);
        ck_assert_ptr_eq(fgets_return, (char *)0x6789);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_ungetc)
{
        FILE *fopen_return;
        int ungetc_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->ungetc.return_value = 6789;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.ungetc = ut_ungetc;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        ungetc_return = ungetc(124, fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->ungetc.ncall, 1);
        ck_assert_int_eq(ut()->ungetc.c, 124);
        ck_assert_ptr_eq(ut()->ungetc.stream, (FILE *)0x1234);
        ck_assert_int_eq(ungetc_return, 6789);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fputc)
{
        FILE *fopen_return;
        int fputc_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fputc.return_value = 6789;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fputc = ut_fputc;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fputc_return = fputc(124, fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fputc.ncall, 1);
        ck_assert_int_eq(ut()->fputc.c, 124);
        ck_assert_ptr_eq(ut()->fputc.stream, (FILE *)0x1234);
        ck_assert_int_eq(fputc_return, 6789);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fputs)
{
        FILE *fopen_return;
        int fputs_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fputs.return_value = 6789;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fputs = ut_fputs;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fputs_return = fputs((const char *) 0x9124, fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fputs.ncall, 1);
        ck_assert_ptr_eq(ut()->fputs.s, (const char *) 0x9124);
        ck_assert_ptr_eq(ut()->fputs.stream, (FILE *)0x1234);
        ck_assert_int_eq(fputs_return, 6789);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fseek)
{
        FILE *fopen_return;
        int fseek_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fseek.return_value = 6789;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fseek = ut_fseek;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fseek_return = fseek(fopen_return, 89, 90);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fseek.ncall, 1);
        ck_assert_ptr_eq(ut()->fseek.stream, (FILE *)0x1234);
        ck_assert_int_eq(ut()->fseek.offset, 89);
        ck_assert_int_eq(ut()->fseek.whence, 90);
        ck_assert_int_eq(fseek_return, 6789);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_ftell)
{
        FILE *fopen_return;
        long ftell_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->ftell.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.ftell = ut_ftell;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        ftell_return = ftell(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->ftell.ncall, 1);
        ck_assert_ptr_eq(ut()->ftell.stream, (FILE *)0x1234);
        ck_assert_int_eq(ftell_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fseeko)
{
        FILE *fopen_return;
        int fseeko_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fseeko.return_value = 6789;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fseeko = ut_fseeko;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fseeko_return = fseeko(fopen_return, 89, 90);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fseeko.ncall, 1);
        ck_assert_ptr_eq(ut()->fseeko.stream, (FILE *)0x1234);
        ck_assert_int_eq(ut()->fseeko.offset, 89);
        ck_assert_int_eq(ut()->fseeko.whence, 90);
        ck_assert_int_eq(fseeko_return, 6789);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_ftello)
{
        FILE *fopen_return;
        off_t ftello_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->ftello.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.ftello = ut_ftello;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        ftello_return = ftello(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->ftello.ncall, 1);
        ck_assert_ptr_eq(ut()->ftello.stream, (FILE *)0x1234);
        ck_assert_int_eq(ftello_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_rewind)
{
        FILE *fopen_return;

        ut()->fopen.return_value= (FILE *)0x1234;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.rewind = ut_rewind;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        rewind(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->rewind.ncall, 1);
        ck_assert_ptr_eq(ut()->rewind.stream, (FILE *)0x1234);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fgetpos)
{
        FILE *fopen_return;
        int fgetpos_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fgetpos.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fgetpos = ut_fgetpos;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fgetpos_return = fgetpos(fopen_return, (fpos_t *)0x9876);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fgetpos.ncall, 1);
        ck_assert_ptr_eq(ut()->fgetpos.pos, (fpos_t *)0x9876);
        ck_assert_ptr_eq(ut()->fgetpos.stream, (FILE *)0x1234);
        ck_assert_int_eq(fgetpos_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fsetpos)
{
        FILE *fopen_return;
        int fsetpos_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fsetpos.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fsetpos = ut_fsetpos;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fsetpos_return = fsetpos(fopen_return, (const fpos_t *)0x9876);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fsetpos.ncall, 1);
        ck_assert_ptr_eq(ut()->fsetpos.pos, (const fpos_t *)0x9876);
        ck_assert_ptr_eq(ut()->fsetpos.stream, (FILE *)0x1234);
        ck_assert_int_eq(fsetpos_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_clearerr)
{
        FILE *fopen_return;

        ut()->fopen.return_value= (FILE *)0x1234;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.clearerr = ut_clearerr;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        clearerr(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->clearerr.ncall, 1);
        ck_assert_ptr_eq(ut()->clearerr.stream, (FILE *)0x1234);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_feof)
{
        FILE *fopen_return;
        int feof_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->feof.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.feof = ut_feof;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        feof_return = feof(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->feof.ncall, 1);
        ck_assert_ptr_eq(ut()->feof.stream, (FILE *)0x1234);
        ck_assert_int_eq(feof_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_ferror)
{
        FILE *fopen_return;
        int ferror_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->ferror.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.ferror = ut_ferror;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        ferror_return = ferror(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->ferror.ncall, 1);
        ck_assert_ptr_eq(ut()->ferror.stream, (FILE *)0x1234);
        ck_assert_int_eq(ferror_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_fileno)
{
        FILE *fopen_return;
        int fileno_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->fileno.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.fileno = ut_fileno;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        fileno_return = fileno(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->fileno.ncall, 1);
        ck_assert_ptr_eq(ut()->fileno.stream, (FILE *)0x1234);
        ck_assert_int_eq(fileno_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_getw)
{
        FILE *fopen_return;
        int getw_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->getw.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.getw = ut_getw;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        getw_return = getw(fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->getw.ncall, 1);
        ck_assert_ptr_eq(ut()->getw.stream, (FILE *)0x1234);
        ck_assert_int_eq(getw_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

START_TEST(test_putw)
{
        FILE *fopen_return;
        int putw_return;

        ut()->fopen.return_value= (FILE *)0x1234;
        ut()->putw.return_value = 765;

        stub_funcs.fopen = ut_fopen;
        stub_funcs.putw = ut_putw;
        stub_funcs.fclose = ut_fclose;
        cstreamstub_set_funcs(&stub_funcs);

        stub_config.activation.always = true;
        cstreamstub_set_config(&stub_config);

        fopen_return = fopen("/tmp/pouet", "r");
        putw_return = putw(17, fopen_return);
        fclose(fopen_return);

        ck_assert_int_eq(ut()->fopen.ncall, 1);
        ck_assert_int_eq(ut()->putw.ncall, 1);
        ck_assert_int_eq(ut()->putw.w, 17);
        ck_assert_ptr_eq(ut()->putw.stream, (FILE *)0x1234);
        ck_assert_int_eq(putw_return, 765);
        ck_assert_int_eq(ut()->fclose.ncall, 1);
}
END_TEST

struct Suite *get_suite(void)
{
        struct Suite *s;
        struct TCase *tc;

        s = suite_create("File back-end");

        /* test cases */
        tc = tcase_create("init");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_init_disabled);
        tcase_add_test(tc, test_init_always);
        tcase_add_test(tc, test_init_with_path_prefix);
        tcase_add_test(tc, test_init_with_tmpfile);
        tcase_add_test(tc, test_init_with_no_path_prefix);
        tcase_add_test(tc, test_init_with_mode_prefix);
        tcase_add_test(tc, test_init_with_no_mode_prefix);
        tcase_add_test(tc, test_init_with_callback);
        tcase_add_test(tc, test_init_with_bad_callback);
        tcase_add_test(tc, test_init_errors_no_funcs);
        tcase_add_test(tc, test_init_errors_no_config);
        suite_add_tcase(s, tc);

        tc = tcase_create("open_close");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_fopen_fclose);
        tcase_add_test(tc, test_freopen_fclose);
        tcase_add_test(tc, test_popen_pclose);
        tcase_add_test(tc, test_fmemopen_fclose);
        tcase_add_test(tc, test_open_memstream_fclose);
        tcase_add_test(tc, test_fdopen_fclose);
        suite_add_tcase(s, tc);

        tc = tcase_create("buffering");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_setbuf);
        tcase_add_test(tc, test_setbuffer);
        tcase_add_test(tc, test_setlinebuf);
        tcase_add_test(tc, test_setvbuf);
        suite_add_tcase(s, tc);

        tc = tcase_create("flush");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_fflush);
        suite_add_tcase(s, tc);

        tc = tcase_create("binary_io");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_fread);
        tcase_add_test(tc, test_fwrite);
        suite_add_tcase(s, tc);

        tc = tcase_create("formated_io");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_vfprintf);
        tcase_add_test(tc, test_vfscanf);
        suite_add_tcase(s, tc);

        tc = tcase_create("row_io");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_fgetc);
        tcase_add_test(tc, test_fgets);
        tcase_add_test(tc, test_ungetc);
        tcase_add_test(tc, test_fputc);
        tcase_add_test(tc, test_fputs);
        suite_add_tcase(s, tc);

        tc = tcase_create("reposition");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_fseek);
        tcase_add_test(tc, test_ftell);
        tcase_add_test(tc, test_fseeko);
        tcase_add_test(tc, test_ftello);
        tcase_add_test(tc, test_rewind);
        tcase_add_test(tc, test_fgetpos);
        tcase_add_test(tc, test_fsetpos);
        suite_add_tcase(s, tc);

        tc = tcase_create("check&reset");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_clearerr);
        tcase_add_test(tc, test_feof);
        tcase_add_test(tc, test_ferror);
        tcase_add_test(tc, test_fileno);

        suite_add_tcase(s, tc);

        tc = tcase_create("word_io");
        tcase_add_checked_fixture(tc, setup, teardown);

        tcase_add_test(tc, test_getw);
        tcase_add_test(tc, test_putw);

        suite_add_tcase(s, tc);

        return s;
}
