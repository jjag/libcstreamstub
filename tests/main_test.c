/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of libcstreamstub. */

/* libcstreamstub is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* libcstreamstub is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with libcstreamstub.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <check.h>

/* Suite creation functions */
/* (defined in other .c files -- Whatever it is !) */

#include <stdio.h>
#include <getopt.h>

struct Suite *get_suite(void);

void print_usage(const char *sw_name);
void print_usage(const char *sw_name)
{
        fprintf(stderr,
                "Usage: %s [-t test_case]\n", sw_name);
        fprintf(stderr,
                "    CK_VERBOSITY env variable influences level of logs\n");
        fprintf(stderr, "       (can be \"silent\", \"minimal\","
                " \"normal\" (default), or \"verbose\")\n");
        fprintf(stderr,
                "    CK_FORK env variable controls whereas there is a "
                "fork for each test\n");
        fprintf(stderr, "       (can be \"no\" or \"yes\" (default))\n");
        fprintf(stderr, "    -t test_case: runs only \"test_case\"\n");
        fprintf(stderr,
                "       (Can be also controled by CK_RUN_CASE"
                " env variable)\n");
}
/* The main() function for setting up and running the tests.
 * Returns a EXIT_SUCCESS on successful running and EXIT_FAILURE on
 * failure.
 */
int main(int argc, char *argv[])
{
        int number_failed;
        int option;

        struct Suite *s;
        struct SRunner *sr;
        const char *tcname = NULL;

        while ((option = getopt(argc, argv,"ht:")) != -1) {
                switch (option) {
                case 'h' :
                        print_usage("test_cstreamstub");
                        exit(EXIT_FAILURE);
                        break;
                case 't' :
                        tcname = optarg;
                        break;
                default: print_usage(argv[0]);
                        exit(EXIT_FAILURE);
                }
        }

        s = get_suite();
        sr = srunner_create(s);

        srunner_run(sr, "File back-end", tcname, CK_ENV);

        number_failed = srunner_ntests_failed(sr);
        srunner_free(sr);

        return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
