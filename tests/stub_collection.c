/* Copyright 2018 Jérôme Arbez-Gindre */

/* This file is part of libcstreamstub. */

/* libcstreamstub is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version. */

/* libcstreamstub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with libcstreamstub.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <strings.h>
#include "stub_collection.h"

static struct ut the_ut_data;

struct ut *ut()
{
        return &the_ut_data;
}

void init_ut_data()
{
        bzero(ut(), sizeof(struct ut));

        ut()->fopen.path = "";
        ut()->fopen.mode = "";
        ut()->freopen.pathname = "";
        ut()->freopen.mode = "";
        ut()->popen.command = "";
        ut()->popen.type = "";
        ut()->fmemopen.mode = "";
        ut()->fdopen.mode = "";
}

FILE *ut_fopen(const char *path, const char *mode)
{
        ut()->fopen.path = path;
        ut()->fopen.mode = mode;
        ut()->fopen.ncall++;
        return ut()->fopen.return_value;
}

FILE *ut_tmpfile(void)
{
        ut()->tmpfile.ncall++;
        return ut()->tmpfile.return_value;
}

int ut_fclose(FILE *stream)
{
        ut()->fclose.ncall++;
        ut()->fclose.stream = stream;
        return ut()->fclose.return_value;
}

FILE *ut_freopen(const char *pathname, const char *mode, FILE *stream)
{
        ut()->freopen.ncall++;
        ut()->freopen.pathname = pathname;
        ut()->freopen.mode = mode;
        ut()->freopen.stream = stream;
        return ut()->freopen.return_value;
}

FILE *ut_popen(const char *command, const char *type)
{
        ut()->popen.ncall++;
        ut()->popen.command = command;
        ut()->popen.type = type;
        return ut()->popen.return_value;
}

int ut_pclose(FILE *stream)
{
        ut()->pclose.ncall++;
        ut()->pclose.stream = stream;
        return ut()->pclose.return_value;
}

FILE *ut_fmemopen(void *buf, size_t size, const char *mode)
{
        ut()->fmemopen.ncall++;
        ut()->fmemopen.buf = buf;
        ut()->fmemopen.size = size;
        ut()->fmemopen.mode = mode;
        return ut()->fmemopen.return_value;
}

FILE *ut_open_memstream(char **ptr, size_t * sizeloc)
{
        ut()->open_memstream.ncall++;
        ut()->open_memstream.ptr = ptr;
        ut()->open_memstream.sizeloc = sizeloc;
        return ut()->open_memstream.return_value;
}

void ut_setbuf(FILE *stream, char *buf)
{
        ut()->setbuf.ncall++;
        ut()->setbuf.stream = stream;
        ut()->setbuf.buf = buf;
        return;
}

void ut_setbuffer(FILE *stream, char *buf, size_t size)
{
        ut()->setbuffer.ncall++;
        ut()->setbuffer.stream = stream;
        ut()->setbuffer.buf = buf;
        ut()->setbuffer.size = size;
        return;
}

void ut_setlinebuf(FILE *stream)
{
        ut()->setlinebuf.ncall++;
        ut()->setlinebuf.stream = stream;
        return;
}

int ut_setvbuf(FILE *stream, char *buf, int mode, size_t size)
{
        ut()->setvbuf.ncall++;
        ut()->setvbuf.stream = stream;
        ut()->setvbuf.buf = buf;
        ut()->setvbuf.mode = mode;
        ut()->setvbuf.size = size;

        return ut()->setvbuf.return_value;
}

FILE *ut_fdopen(int fd, const char *mode)
{
        ut()->fdopen.ncall++;
        ut()->fdopen.fd = fd;
        ut()->fdopen.mode = mode;
        return ut()->fdopen.return_value;
}

int ut_fflush(FILE *stream)
{
        ut()->fflush.ncall++;
        ut()->fflush.stream = stream;
        return ut()->fflush.return_value;
}

size_t ut_fread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
        ut()->fread.ncall++;
        ut()->fread.ptr = ptr;
        ut()->fread.size = size;
        ut()->fread.nmemb = nmemb;
        ut()->fread.stream = stream;
        return ut()->fread.return_value;
}

size_t ut_fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
        ut()->fwrite.ncall++;
        ut()->fwrite.ptr = ptr;
        ut()->fwrite.size = size;
        ut()->fwrite.nmemb = nmemb;
        ut()->fwrite.stream = stream;
        return ut()->fwrite.return_value;
}

int ut_vfprintf(FILE *stream, const char *format, va_list ap)
{
        ut()->vfprintf.ncall++;
        ut()->vfprintf.stream = stream;
        vsnprintf(ut()->vfprintf.str_result_buffer,
                  ut()->vfprintf.str_result_buffer_size,
                  format, ap);

        return ut()->vfprintf.return_value;
}

int ut_vfscanf(FILE *stream, const char *format, va_list ap)
{
        ut()->vfscanf.ncall++;
        ut()->vfscanf.stream = stream;
        vsscanf(ut()->vfscanf.read_string, format, ap);

        return ut()->vfscanf.return_value;
}

int ut_fgetc(FILE *stream)
{
        ut()->fgetc.ncall++;
        ut()->fgetc.stream = stream;
        return ut()->fgetc.return_value;
}

char *ut_fgets(char *s, int size, FILE *stream)
{
        ut()->fgets.ncall++;
        ut()->fgets.stream = stream;
        ut()->fgets.s = s;
        ut()->fgets.size = size;
        return ut()->fgets.return_value;
}

int ut_ungetc(int c, FILE *stream)
{
        ut()->ungetc.ncall++;
        ut()->ungetc.stream = stream;
        ut()->ungetc.c = c;
        return ut()->ungetc.return_value;
}

int ut_fputc(int c, FILE *stream)
{
        ut()->fputc.ncall++;
        ut()->fputc.stream = stream;
        ut()->fputc.c = c;
        return ut()->fputc.return_value;
}

int ut_fputs(const char *s, FILE *stream)
{
        ut()->fputs.ncall++;
        ut()->fputs.s = s;
        ut()->fputs.stream = stream;
        return ut()->fputs.return_value;
}

int ut_fseek(FILE *stream, long offset, int whence)
{
        ut()->fseek.ncall++;
        ut()->fseek.stream = stream;
        ut()->fseek.whence = whence;
        ut()->fseek.offset = offset;
        return ut()->fseek.return_value;
}

long ut_ftell(FILE *stream)
{
        ut()->ftell.ncall++;
        ut()->ftell.stream = stream;
        return ut()->ftell.return_value;
}

int ut_fseeko(FILE *stream, off_t offset, int whence)
{
        ut()->fseeko.ncall++;
        ut()->fseeko.stream = stream;
        ut()->fseeko.whence = whence;
        ut()->fseeko.offset = offset;
        return ut()->fseeko.return_value;
}

off_t ut_ftello(FILE *stream)
{
        ut()->ftello.ncall++;
        ut()->ftello.stream = stream;
        return ut()->ftello.return_value;
}

void ut_rewind(FILE *stream)
{
        ut()->rewind.ncall++;
        ut()->rewind.stream = stream;
        return;
}

int ut_fgetpos(FILE *stream, fpos_t *pos)
{
        ut()->fgetpos.ncall++;
        ut()->fgetpos.stream = stream;
        ut()->fgetpos.pos = pos;
        return ut()->fgetpos.return_value;
}

int ut_fsetpos(FILE *stream, const fpos_t *pos)
{
        ut()->fsetpos.ncall++;
        ut()->fsetpos.stream = stream;
        ut()->fsetpos.pos = pos;
        return ut()->fsetpos.return_value;
}

void ut_clearerr(FILE *stream)
{
        ut()->clearerr.ncall++;
        ut()->clearerr.stream = stream;
        return;
}

int ut_feof(FILE *stream)
{
        ut()->feof.ncall++;
        ut()->feof.stream = stream;
        return ut()->feof.return_value;
}

int ut_ferror(FILE *stream)
{
        ut()->ferror.ncall++;
        ut()->ferror.stream = stream;
        return ut()->ferror.return_value;
}

int ut_fileno(FILE *stream)
{
        ut()->fileno.ncall++;
        ut()->fileno.stream = stream;
        return ut()->fileno.return_value;
}

int ut_getw(FILE *stream)
{
        ut()->getw.ncall++;
        ut()->getw.stream = stream;
        return ut()->getw.return_value;
}

int ut_putw(int w, FILE *stream)
{
        ut()->putw.ncall++;
        ut()->putw.w = w;
        ut()->putw.stream = stream;
        return ut()->putw.return_value;
}
